angular.module('starter.services', [])

.service('ProductService', function($http,pic1, pic2, pic3, pic4, pic5) {
    
    return {
        
        GetUserLoans: function(){
            return $http({
                method: 'GET',
                url: 'http://localhost:41946/umbraco/api/product/GetUserLoans?userid=' + 1068,
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        },
        ReturnProduct: function(productid, loanid){
            return $http({
                method: 'POST',
                url: 'http://localhost:41946/umbraco/api/product/returnproduct?productid=' + productid + '&loanid=' + loanid + "&userid=" + 1068,
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        },
        //Returnerer et enkelt objekt
        GetProduct: function(productid){
            return $http({
                method: 'GET',
                url: 'http://localhost:41946/umbraco/api/product/getproduct?productid=' + productid 
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        },
        //Returnerer alle objekter
        GetAllProducts: function(){
            return $http({
                method: 'GET',
                url: 'http://localhost:41946/umbraco/api/product/getallproducts'
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        },
        CreateNewLoan: function(productId, loanEndDate){
            return $http({
                method: 'POST',
                url: 'http://localhost:41946/umbraco/api/product/createnewloan?userid=' + 1068 + '&productid=' + productId + '&loanenddate=' + loanEndDate,
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        },
        CheckoutProduct : function(product){
            for (var i = 0; i < products.length; i++) {
                if (products[i].productid === parseInt(productid)) {
                    products[i]("productIsAvailable: true")
                }
            }
        },
        //Opdaterer et objekt
        UpdateItem(product){
            
        }
        
    }
})