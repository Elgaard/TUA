angular.module('starter.controllers', [])

    .controller('DashCtrl', function($scope, $location) {
        $scope.user = {
            userid: 1068
        }

        $scope.go = function(path) {
            $location.path(path);
        }


    })

    .controller('DevicesCtrl', function($scope, $rootScope, $state, ProductService) {

        //Vi opretter en model, vi kalder items. Modellen fastgøres til ItemCtrl scopet.
        //Husk at indsætte ItemService en en del af parameterlisten.
        ProductService.GetAllProducts().then(function(products) {
            $scope.products = products;
        });

        //Denne benyttes når vi kommer fra en anden tilstand og skal opdatere productsmodel
        $rootScope.$on('updateProductsModel', function(event, args) {
            ProductService.GetAllProducts().then(function(products) {
                $scope.products = products;
            });
        });
    })

    .controller('DeviceCtrl', function($scope, $stateParams, $state, $rootScope, ProductService) {

        //Vi opretter en model, vi kalder items. Modellen fastgøres til ItemCtrl scopet.
        //Husk at indsætte ItemService en en del af parameterlisten.
        ProductService.GetProduct($stateParams.productid).then(function(product) {
            $scope.product = product;
        });


        $scope.createNewLoan = function(productid, loanenddate) {
            ProductService.CreateNewLoan(productid, loanenddate).then(function() {
                //Når lånet er oprettet kan vi skifte state og update productsmodel
                $state.go('tab.products');
                $rootScope.$broadcast("updateProductsModel");
            });


        };
        //$scope.checkoutProduct = function(product){
        //    ProductService.CheckoutProduct(product);
        //}


    })

    .controller('AccountCtrl', function($scope) { })

    .controller('ContactAdminCtrl', function($scope) { })

    .controller('SplashCtrl', function($scope) { })

    .controller('MyRentedCtrl', function($scope) { })

    .controller('ReturnDeviceCtrl', function($scope) { })

    .controller('RentalHistoryCtrl', function($scope) { })

    .controller('SplashCtrl', function($scope, $location) {
        $scope.go = function(path) {
            $location.path(path);
        }
    })

    .controller('LoginCtrl', function($scope, $location) {
        $scope.go = function(path) {
            $location.path(path);
        }
    })

    .controller('ForgotPassCtrl', function($scope) { })