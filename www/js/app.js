// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

    .run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            if (window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    })

    .config(function($stateProvider, $urlRouterProvider) {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider

            .state('splash', {
                url: '/splash',
                templateUrl: 'templates/splash.html',
                controller: 'SplashCtrl'
            })

            .state('login', {
                url: '/login',
                templateUrl: 'templates/loginmedlem.html',
                controller: 'SignInCtrl'
            })

            .state('forgottenpass', {
                url: '/forgottenpass',
                templateUrl: 'templates/glemtpass.html',
                controller: 'ForgotPassCtrl'
            })




            // setup an abstract state for the tabs directive
            .state('tab', {
                url: '/tab',
                abstract: true,
                templateUrl: 'templates/tabs.html'
            })

            // Each tab has its own nav history stack:

            .state('tab.splash', {
                url: '/splash',
                views: {
                    'tab-splash': {
                        templateUrl: 'templates/splash.html',
                        controller: 'SplashCtrl'
                    }
                }
            })


            .state('tab.dash', {
                url: '/dash',
                views: {
                    'tab-dash': {
                        templateUrl: 'templates/dashboard.html',
                        controller: 'DashCtrl'
                    }
                }
            })


            .state('tab.login', {
                url: '/splash/login',
                views: {
                    'tab-splash': {
                        templateUrl: 'templates/loginmedlem.html',
                        controller: 'LoginCtrl'
                    }
                }
            })

            .state('tab.contactadmin', {
                url: '/contactadmin',
                views: {
                    'tab-contactadmin': {
                        templateUrl: 'templates/kontaktadmin.html',
                        controller: 'ContactAdminCtrl'
                    }
                }
            })

            .state('tab.account', {
                url: '/account',
                views: {
                    'tab-account': {
                        templateUrl: 'templates/dinkonto.html',
                        controller: 'AccountCtrl'
                    }
                }
            })

            .state('tab.devices', {
                url: '/dash/devices',
                views: {
                    'tab-dash': {
                        templateUrl: 'templates/enheder.html',
                        controller: 'DevicesCtrl'
                    }
                }

            })

            .state('tab.device', {
                url: '/device/id/:productid',
                views: {
                    'tab-dash': {
                        templateUrl: 'templates/enhed.html',
                        controller: 'DeviceCtrl'
                    }
                }

            })


            .state('tab.myrented', {
                url: '/dash/myrented',
                views: {
                    'tab-dash': {
                        templateUrl: 'templates/seudlaan.html',
                        controller: 'MyRentedCtrl'
                    }
                }
            })

            .state('tab.returndevice', {
                url: '/dash/returndevice',
                views: {
                    'tab-dash': {
                        templateUrl: 'templates/seudlaan.html',
                        controller: 'ReturnDeviceCtrl'
                    }
                }
            })

            .state('tab.rentalhistory', {
                url: '/dash/rentalhistory',
                views: {
                    'tab-dash': {
                        templateUrl: 'templates/seudlaan.html',
                        controller: 'RentalHistoryCtrl'
                    }
                }
            })

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/splash');

    });